package Exemple2_15_ProdCons_Inicial;

public class Main {

	public static void main(String[] args){
		System.out.println("[Fil ppal] Versió Productor-Consumidor amb 1 element");
		Dada dada = new Dada(3000);

		int numElements = 5;			//Num elements a produir i consumir
		Thread productor = new Thread(new Productor(dada, numElements), "Productor");
		Thread consumidor = new Thread(new Consumidor(dada, numElements), "Consumidor");

		productor.start();
		consumidor.start();
	}//main
}//class
