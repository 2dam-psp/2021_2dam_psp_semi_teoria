
public class Act2_3_TicTac2  implements Runnable{
	//Constructor --------------------------------
	public Act2_3_TicTac2 (){
	}

	//Mètode Run -----------------------------------
	public void run(){
		int i = 0;
		while (i < 5){	//bucle de 5 iteracions
			try {	
				Thread.sleep(2000);	//espera d'1 segon
			} catch (InterruptedException e) {
				e.printStackTrace();
			}//catch
			System.out.print (" " +Thread.currentThread().getName()+ " ");   //mostra el nom
			i++;
		}//while
	}//run

	//Métode main -------------------------------
	public static void main(String[] args){

		System.out.println("[Fil ppal] Iniciant rellotge...");

		Act2_3_TicTac2 objRun1 =  new Act2_3_TicTac2(); //creacio nou objecte Runnable1
		Act2_3_TicTac2 objRun2 =  new Act2_3_TicTac2(); //creacio nou objecte Runnable1
		
		Thread objFil1 = new Thread(objRun1,"TIC");
		Thread objFil2 = new Thread(objRun2,"TAC");

		objFil1.start(); //inici fil1
		try {
			Thread.sleep(1000);	//espera 0'5segons per iniciar 2n fil
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		objFil2.start(); //iniciar fil2

		try {
			objFil1.join();	//fil ppal espera acabament fil1
			objFil2.join(); //fil ppal espera acabament fil2
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		System.out.println("\n[Fil ppal] Finalitzat rellotge");
	}//main
}//class
