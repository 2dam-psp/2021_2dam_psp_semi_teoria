
public class Act2_1_InfoFil  extends Thread{
	
	//Constructor --------------------------------
	public Act2_1_InfoFil (String NomFil, Boolean ferDimoni){
		this.setName(NomFil);
		if (ferDimoni)
			this.setDaemon(true);
	}

	//Mètode Run -----------------------------------
	public void run(){
		
		System.out.println("INFORMACIO DEL FIL "+ this.getName());
		System.out.println("["+ this.getName() + "]" +" ID del fil: " + this.getId());
		System.out.println("["+ this.getName() + "]" +" Nom del fil: " + this.getName());
		System.out.println("["+ this.getName() + "]" +" Prioritat del fil: " + this.getPriority());
		System.out.println("["+ this.getName() + "]" +" Es viu el fil? " + this.isAlive());
		System.out.println("["+ this.getName() + "]" +" Es interromput el fil? " + this.isInterrupted());
		
		System.out.println("["+ this.getName() + "]" +" Es un dimoni el fil? " + this.isDaemon());
		System.out.println("["+ this.getName() + "] ----");
	}//run

	//Métode main -------------------------------
	public static void main(String[] args){
		 
		Act2_1_InfoFil objFil1 =  new Act2_1_InfoFil("Fil1",true); //creacio nou fil1
		Act2_1_InfoFil objFil2 =  new Act2_1_InfoFil("Fil2",false); //creacio nou fil2

		System.out.println("[Fil ppal] Iniciant fils...");
		
		objFil1.start(); //inici fil1
		objFil2.start(); //iniciar fil2

		try {
			objFil1.join();	//fil ppal espera acabament fil1
			objFil2.join(); //fil ppal espera acabament fil2
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		System.out.println("\n[Fil ppal] Finalitzat");
	}//main
}//class
