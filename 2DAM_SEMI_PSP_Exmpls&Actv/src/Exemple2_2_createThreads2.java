
public class Exemple2_2_createThreads2  implements Runnable{

	//Propietats --------------------------------
	private String nomFil;

	//Constructor --------------------------------
	public Exemple2_2_createThreads2 (String NomFil){
		this.nomFil = NomFil;
	}//constructor

	//Mètode Run -----------------------------------
	public void run(){
		String nomActual = Thread.currentThread().getName();
		System.out.println ("Fil["+ nomActual + "] -> Hola, soc el fil: "+ nomActual);
		System.out.println ("Fil["+ nomActual + "] -> Fil "+ nomActual + " ha acabat");
	}//run

	//Métode main -------------------------------
	public static void main(String[] args){
		System.out.println("[Fil ppal] -> Comença el fil ppal \n");
		//creacio obj1 Runnable
		Exemple2_2_createThreads2 objRnb1 = new Exemple2_2_createThreads2("nomFil1");
		//creacio obj2 Runnable
		Exemple2_2_createThreads2 objRnb2 = new Exemple2_2_createThreads2("nomFil2"); 
		Thread objFil1 = new Thread(objRnb1, objRnb1.nomFil);	//creacio nou fil1
		Thread objFil2 = new Thread(objRnb2, objRnb2.nomFil);	//creacio nou fil2
		
		System.out.println("[Fil ppal] -> Executant nous fils..");
		objFil1.start();
		objFil2.start();
		try {
			objFil1.join();
			objFil2.join();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("[Fil ppal] -> Acabat");
	} //main
}//class

