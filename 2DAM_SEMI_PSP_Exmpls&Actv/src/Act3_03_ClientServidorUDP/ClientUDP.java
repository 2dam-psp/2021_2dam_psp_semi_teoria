package Act3_03_ClientServidorUDP;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.Scanner;

public class ClientUDP implements Runnable{

	
	ClientUDP(){
    	System.out.println("["+Thread.currentThread().getName()+"] Creat el ClientUDP");
    }
    
    //---------------------------
	public void run() {
		try {
			
			System.out.println("["+Thread.currentThread().getName()+"] Introdueix el text a enviar: ");
			
			Scanner s = new Scanner(System.in);
			//Message to send
			String cadenaAenviar = s.nextLine();

			//Showing text to send through DatagramPacket
			System.out.println("["+Thread.currentThread().getName()+"] "+ "Text a enviar al Servidor: "+ cadenaAenviar);

			//conversion of String to bytes[]
			byte[] missatge = cadenaAenviar.getBytes("UTF-8");
			//setting destiny port
			int portDesti = 10000;
			//setting destiny IP
			InetAddress ipDesti = InetAddress.getByName("127.0.0.1");

			//New DatagramSocket to insert DatagramPacket
			DatagramSocket lsocket = new DatagramSocket();
			
			//New DatagramPacket
			DatagramPacket datagrama = new DatagramPacket(missatge, missatge.length, ipDesti, portDesti);
			
			//Sending DatagramPacket
			System.out.println("["+Thread.currentThread().getName()+"] Enviant datagrama al port " + portDesti + "...");
			lsocket.send(datagrama);

			//Close socket
			if(!lsocket.isClosed())
				lsocket.close();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}//run
}//class
