package Act3_03_ClientServidorUDP;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		try {
		System.out.println("Programa que envia el capturat per teclat pel Client UDP al Servidor UDP");
		System.out.println("========================================================================");
		
		Thread clientUDP = new Thread(new ClientUDP(), "ClientUDP");
		Thread serverUDP = new Thread(new ServidorUDP(), "ServerUDP");
		
		serverUDP.start();
		Thread.sleep(2000);
		clientUDP.start();
		
		clientUDP.join();
		serverUDP.join();
		
		System.out.println("Programa finalitzat");
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}//main
}//class
