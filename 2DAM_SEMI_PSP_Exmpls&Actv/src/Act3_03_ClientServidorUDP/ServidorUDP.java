package Act3_03_ClientServidorUDP;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

public class ServidorUDP implements Runnable{
	
    ServidorUDP(){
    	System.out.println("["+Thread.currentThread().getName()+"] Creat el ServidorUDP");
    }
    
    //---------------------------
	@Override
	public void run() {
		try {
			//New buffer to get message
			byte[] bytesArebre = new byte[1024];
		
			//setting destiny port
			int portDesti = 10000;

			//New empty DatagramPacket
			DatagramPacket datagrama = new DatagramPacket(bytesArebre, bytesArebre.length);

			//New DatagramSocket
			DatagramSocket lsocket = new DatagramSocket(portDesti);
			
			System.out.println("["+Thread.currentThread().getName()+"] Esperant datagrama al port " + portDesti + "...");
			//Receiving DatagramPacket
			lsocket.receive(datagrama);

			//Close socket
			if(!lsocket.isClosed())
				lsocket.close();

			//Showing received text from DatagramPacket
			System.out.println("["+Thread.currentThread().getName()+"] Text rebut: " + new String(datagrama.getData(), 0, datagrama.getLength(), "UTF-8"));


		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}//run
}//class
