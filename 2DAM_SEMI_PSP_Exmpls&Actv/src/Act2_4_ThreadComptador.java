
public class Act2_4_ThreadComptador extends Thread{
	//Propietats --------------------------------
	private int comptador = 0; //contador fil
	//private String nomFil;

	//Constructor --------------------------------
	public Act2_4_ThreadComptador (String NomFil){
		this.setName(NomFil);
	}//fi constructor

	//run -----------------------------------
	public void run(){
		while (comptador < 5){			//itera 5 vegades 
			System.out.println ("["+ this.getName()+ "] Comptador = " + comptador);
			comptador++;
		}//while
	}//run

	//Métode main -------------------------------
	public static void main(String[] args){
		try {
			String prefixNomFil = "Fil";
			Act2_4_ThreadComptador objFil = null;	//declaracio de l'objecte Thread
			System.out.println("[Fil ppal] Comptador amb fils\n");

			Thread[] fils = new Thread[3];	//vector dels 3 fils
			//bucle
			for (int i=0; i<3; i++){			//per a cada fil, el crea i l'executa
				System.out.println("[Fil ppal] Creant nou fil "+ prefixNomFil + i);
				objFil = new Act2_4_ThreadComptador(prefixNomFil+i); 	//creacio nou fil
				System.out.println("[Fil ppal] Executant nou fil "+ prefixNomFil  + i);
				objFil.start(); 		//iniciar fil
				fils[i] = objFil;		//Desa la referencia del fil al vector
			}
			System.out.println("[Fil ppal] 3 fils creats...");

			//Fil ppal espera finalització dels fils del vector
			for (Thread fil : fils)
				fil.join();

			System.out.println("[Fil ppal] els 3 fils han finalitzat");
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}//catch
	} //main
}//class
