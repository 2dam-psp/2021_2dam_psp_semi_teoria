package Act3_04_dialegClientServidorUDPambTancament;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		try {
		System.out.println("Programa que implementa un diàleg entre un Client UDP al Servidor UDP "
				+ "finalitzant només quan algun del dos escriga la paraula de tancament acordada");
		System.out.println("========================================================================");

		//captura paraula tancament
		System.out.println("["+Thread.currentThread().getName()+"] Introdueix la paraula de tancament> ");
		Scanner s = new Scanner(System.in);
		String cadenaTancament = s.nextLine();
		
		//captura port on escoltar el Servidor
		System.out.println("["+Thread.currentThread().getName()+"] Port on escoltar el Servidor conversa> ");
		int portServidor = s.nextInt();
		
		//captura port on escoltar el Client
		System.out.println("["+Thread.currentThread().getName()+"] Port on escoltar el Client conversa> ");
		int portClient = s.nextInt();
		
		//tancament lectura teclat
		//s.close();
		
		Thread clientUDP = new Thread(new ClientConversaUDP(portClient, portServidor, cadenaTancament), "ClientUDP");
		Thread serverUDP = new Thread(new ServidorConversaUDP(portServidor, portClient, cadenaTancament), "ServerUDP");
		
		serverUDP.start();
		Thread.sleep(2000);
		clientUDP.start();
		
		clientUDP.join();
		serverUDP.join();
		
		System.out.println("Programa finalitzat");
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}//main
}//class
