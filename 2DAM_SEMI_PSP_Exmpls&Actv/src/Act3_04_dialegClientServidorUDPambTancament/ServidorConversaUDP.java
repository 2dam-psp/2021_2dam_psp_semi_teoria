package Act3_04_dialegClientServidorUDPambTancament;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.Scanner;

public class ServidorConversaUDP implements Runnable{
	//Attributs
	private int portClient;
	private int portServidor;
	private String cadenaTancament;
	private boolean finalitzar;

	ServidorConversaUDP(int portServidor, int portClient, String cadenaTancament){
		System.out.println("["+Thread.currentThread().getName()+"] Creat el ClientConversaUDP");
		this.portServidor = portServidor;
		this.portClient = portClient;
		this.cadenaTancament = cadenaTancament;
		//indica quan finalitzar el bucle
		this.finalitzar = false;
	}

	//---------------------------
	@Override
	public void run() {
		try {
			//Creacio del DatagramSocket al portClient on rebre el DatagramPacket
			DatagramSocket socReb = new DatagramSocket(portServidor);

			//Creacio del DatagramSocket des del que enviar el DatagramPacket
			DatagramSocket socEnv = new DatagramSocket();

			DatagramPacket datagrama;
			Scanner s = new Scanner(System.in);
			String cadenaAenviar, cadenaRebuda;

			boolean tancarConnexio = false;

			do {
				// RECEPCIO DE DADES +++++++++
				//Creacio d'un buffer on rebre les dades
				byte[] bytesArebre = new byte[1024];

				//Creacio d'un DatagramPacket buit
				datagrama = new DatagramPacket(bytesArebre, bytesArebre.length);

				System.out.println("["+Thread.currentThread().getName()+"] Esperant datagrama al port " + portServidor + "...");
				//Rebent dades sobre el DatagramPacket
				socReb.receive(datagrama);

				//Visualitzaicó del text rebut del DatagramPacket
				cadenaRebuda = new String(datagrama.getData(), 0, datagrama.getLength(), "UTF-8");

				//Detectada cadenaTancament, tancar connexio, finalitzar el bucle i no fer res mes
				tancarConnexio = detectaCadenaTancament(cadenaRebuda);

				if (tancarConnexio) {
					//Tancament del socket
					if(!socReb.isClosed())
						socReb.close();
				}else {

					//Visualització del text rebut del DatagramPacket
					System.out.println("["+Thread.currentThread().getName()+"] Text rebut: " + cadenaRebuda);

					// ENVIAMENT DE DADES  +++++++++
					System.out.println("["+Thread.currentThread().getName()+"] Introdueix el text a enviar> ");

					//Missatge a enviar
					cadenaAenviar = s.nextLine();

					//Visualitzant el missatge a enviar per DatagramPacket
					System.out.println("["+Thread.currentThread().getName()+"] "+ "Text a enviar al Client: "+ cadenaAenviar);

					//Conversio de String a bytes[]
					byte[] missatge = cadenaAenviar.getBytes("UTF-8");

					//Creacio del  DatagramPacket
					datagrama = new DatagramPacket(missatge, missatge.length, InetAddress.getLocalHost(), portClient);

					//Enviament del DatagramPacket
					System.out.println("["+Thread.currentThread().getName()+"] Enviant cadena al port " + portClient + "...");
					socEnv.send(datagrama);

					//Detectada cadenaTancament, tancar connexio, finalitzar el bucle i no fer res mes
					tancarConnexio = detectaCadenaTancament(cadenaAenviar);
					//Detectada cadenaTancament, tancar connexio, finalitzar el bucle i no fer res mes
					if (tancarConnexio) {
						//Tancament del socket
						if(!socEnv.isClosed())
							socEnv.close();
					}
				}
			}while (!finalitzar);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}//run

	//----------------------------
	boolean detectaCadenaTancament(String cadena) {
		if (cadena.contentEquals(cadenaTancament)) {

			System.out.println("\t\t+++++++++++++++++++++++++++++++++");
			System.out.println("["+Thread.currentThread().getName()+"] Detectada cadena tancament "+cadenaTancament+". Dialeg finalitzat");
			System.out.println("\t\t+++++++++++++++++++++++++++++++++");
			finalitzar = true;
			return true;
		}else {
			return false;
		}
	}

}//class
