package Act2_14_LectorsEscriptors;

public class Escriptor implements Runnable{
	//attrib
	int numEscriptures;
	BaseDades bd;

	//Constructor
	public Escriptor (int numEscriptures, BaseDades bd){
		this.numEscriptures = numEscriptures;
		this.bd = bd;
		System.out.println("Creació d'un Escriptor amb "+numEscriptures+" escriptures");
	}

	public void run() {
		for (int i=0; i<numEscriptures; i++){		//per cada escriptura
			System.out.println("[" +Thread.currentThread().getName()+"] Solicita escriptura ");
			bd.escriure(i+1);					//accio d'escriure
		}//for
	}//run
}//class