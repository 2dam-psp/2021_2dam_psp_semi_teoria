package Exemple3_03_DialegSimpleClientServidorUDP;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		try {
		System.out.println("Programa que envia el capturat per teclat pel Client UDP al Servidor UDP"
				+ "\n i el Servidor respon al Client amb la quantitat de caracters enviats"
				+ "\n visualitzant-los el Client per pantalla");
		System.out.println("========================================================================");
		
		int portServidor = 10000;
		int portClient = 9999;
		Thread clientUDP = new Thread(new ClientUDP(portClient, portServidor), "ClientUDP");
		Thread serverUDP = new Thread(new ServidorUDP(portServidor, portClient), "ServerUDP");
		
		serverUDP.start();
		Thread.sleep(2000);
		clientUDP.start();
		
		clientUDP.join();
		serverUDP.join();
		
		System.out.println("Programa finalitzat");
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}//main
}//class
