package Exemple3_03_DialegSimpleClientServidorUDP;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

public class ServidorUDP implements Runnable{
	//Attributs
	private int portServidor;
	private int portClient;
	
    ServidorUDP(int portServidor, int portClient){
    	this.portServidor = portServidor;
    	this.portClient = portClient;
    	System.out.println("["+Thread.currentThread().getName()+"] Creat el ServidorUDP");
    }
    
    //---------------------------
	@Override
	public void run() {
		try {
			// RECEPCIO DE DADES +++++++++
			//Creacio d'un buffer on rebre les dades
			byte[] bytesArebre = new byte[1024];

			//Creacio d'un DatagramPacket buit
			DatagramPacket datagrama = new DatagramPacket(bytesArebre, bytesArebre.length);

			//Creacio del DatagramSocket al port del Servidor on rebre el DatagramPacket
			DatagramSocket lsocket = new DatagramSocket(portServidor);
			
			System.out.println("["+Thread.currentThread().getName()+"] Esperant datagrama al port " + portServidor + "...");
			//Rebent dades sobre el DatagramPacket
			lsocket.receive(datagrama);

			//Tancament del socket
			if(!lsocket.isClosed())
				lsocket.close();

			//Visualització del text rebut del DatagramPacket
			String missatge = new String(datagrama.getData(), 0, datagrama.getLength(), "UTF-8"); 
			System.out.println("["+Thread.currentThread().getName()+"] Text rebut: " + missatge);
			
			// ENVIAMENT DE DADES  +++++++++
			//Creacio del DatagramSocket on inserir el DatagramPacket
			lsocket = new DatagramSocket();
			
			//Creacio del  DatagramPacket
			String numCharsMissatge = String.valueOf(missatge.length());
			datagrama = new DatagramPacket(numCharsMissatge.getBytes("UTF-8"), numCharsMissatge.length(), InetAddress.getByName("127.0.0.1"), portClient);
			
			//Enviament del DatagramPacket
			System.out.println("["+Thread.currentThread().getName()+"] Enviant datagrama al port " + portClient + "...");
			lsocket.send(datagrama);

			//Tancament del socket
			if(!lsocket.isClosed())
				lsocket.close();


		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}//run
}//class
