package Exemple3_03_DialegSimpleClientServidorUDP;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.Scanner;

public class ClientUDP implements Runnable{
	//Attributs
	private int portClient;
	private int portServidor;
	
	ClientUDP(int portClient, int portServidor){
		this.portClient = portClient;
		this.portServidor = portServidor;
    	System.out.println("["+Thread.currentThread().getName()+"] Creat el ClientUDP");
    }
    
    //---------------------------
	public void run() {
		try {
			// ENVIAMENT DE DADES  +++++++++
			System.out.println("["+Thread.currentThread().getName()+"] Introdueix el text a enviar: ");
			
			Scanner s = new Scanner(System.in);
			//Missatge a enviar
			String cadenaAenviar = s.nextLine();

			//Visualitzant el missatge a enviar per DatagramPacket
			System.out.println("["+Thread.currentThread().getName()+"] "+ "Text a enviar al Servidor: "+ cadenaAenviar);

			//Conversio de String a bytes[]
			byte[] missatge = cadenaAenviar.getBytes("UTF-8");
			
			//Creacio del DatagramSocket on inserir el DatagramPacket
			DatagramSocket lsocket = new DatagramSocket();
			
			//Creacio del  DatagramPacket
			DatagramPacket datagrama = new DatagramPacket(missatge, missatge.length, InetAddress.getByName("127.0.0.1"), portServidor);
			
			//Enviament del DatagramPacket
			System.out.println("["+Thread.currentThread().getName()+"] Enviant datagrama al port " + portServidor + "...");
			lsocket.send(datagrama);

			//Tancament del socket
			if(!lsocket.isClosed())
				lsocket.close();
			
			// RECEPCIO DE DADES +++++++++
			//Creacio d'un buffer on rebre les dades
			byte[] bytesArebre = new byte[1024];

			//Creacio d'un DatagramPacket buit
			datagrama = new DatagramPacket(bytesArebre, bytesArebre.length);

			//Creacio del DatagramSocket al port del Client on rebre el DatagramPacket
			lsocket = new DatagramSocket(portClient);
			
			System.out.println("["+Thread.currentThread().getName()+"] Esperant datagrama al port " + portClient + "...");
			//Rebent dades sobre el DatagramPacket
			lsocket.receive(datagrama);

			//Tancament del socket
			if(!lsocket.isClosed())
				lsocket.close();

			//Visualitzaicó del text rebut del DatagramPacket
			String missatgeRebut = new String(datagrama.getData(), 0, datagrama.getLength(), "UTF-8"); 
			System.out.println("["+Thread.currentThread().getName()+"] Text rebut: " + missatgeRebut);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}//run
}//class
