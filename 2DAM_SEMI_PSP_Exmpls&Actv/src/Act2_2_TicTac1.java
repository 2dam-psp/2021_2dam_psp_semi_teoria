
public class Act2_2_TicTac1  extends Thread{
	//Constructor --------------------------------
	public Act2_2_TicTac1 (String NomFil){
		this.setName(NomFil);
	}

	//Mètode Run -----------------------------------
	public void run(){
		int i = 0;
		while (i < 5){	//bucle de 5 iteracions
			try {	
				sleep(2000);	//espera d'1 segon
			} catch (InterruptedException e) {
				e.printStackTrace();
			}//catch
			System.out.print (" " +this.getName()+ " ");   //mostra el nom
			i++;
		}//while
	}//run

	//Métode main -------------------------------
	public static void main(String[] args){

		System.out.println("[Fil ppal] Iniciant rellotge...");

		Act2_2_TicTac1 objFil1 =  new Act2_2_TicTac1("TIC"); //creacio nou fil1
		Act2_2_TicTac1 objFil2 =  new Act2_2_TicTac1("TAC"); //creacio nou fil2

		objFil1.start(); //inici fil1
		try {
			Thread.sleep(1000);	//espera 0'5segons per iniciar 2n fil
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		objFil2.start(); //iniciar fil2

				try {
			objFil1.join();	//fil ppal espera acabament fil1
			objFil2.join(); //fil ppal espera acabament fil2
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		System.out.println("\n[Fil ppal] Finalitzat rellotge");
	}//main
}//class
