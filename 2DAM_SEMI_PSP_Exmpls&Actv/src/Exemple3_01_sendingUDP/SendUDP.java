package Exemple3_01_sendingUDP;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

public class SendUDP {

	public static void main(String[] args) {
		try {
			//Missatge a enviar
			String cadenaAenviar = new String ("Hola món");
			//Visualitzant el missatge a enviar per DatagramPacket
			System.out.print("Text a enviar: " + cadenaAenviar);

			//Conversio de String a bytes[]
			byte[] missatge = cadenaAenviar.getBytes("UTF-8");
			//definint el port destí
			int portDesti = 10000;
			//definint la IP desti
			InetAddress ipDesti = InetAddress.getByName("127.0.0.1");

			//Creacio del DatagramSocket on inserir el DatagramPacket
			DatagramSocket lsocket = new DatagramSocket();
			
			//Creacio del  DatagramPacket
			DatagramPacket datagrama = new DatagramPacket(missatge, missatge.length, ipDesti, portDesti);
			//Enviament del DatagramPacket
			lsocket.send(datagrama);

			//Tancament del socket
			if(!lsocket.isClosed())
				lsocket.close();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}//main
}//class
