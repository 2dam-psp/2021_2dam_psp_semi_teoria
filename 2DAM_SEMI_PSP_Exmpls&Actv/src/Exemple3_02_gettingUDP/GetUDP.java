package Exemple3_02_gettingUDP;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

public class GetUDP {

	public static void main(String[] args) {
		try {
			//Creacio d'un buffer on rebre les dades
			byte[] bytesArebre = new byte[1024];
		
			//definint el port destí
			int portDesti = 10000;

			//Creacio d'un DatagramPacket buit
			DatagramPacket datagrama = new DatagramPacket(bytesArebre, bytesArebre.length);

			//Creacio del DatagramSocket al port destí on rebre el DatagramPacket
			DatagramSocket lsocket = new DatagramSocket(portDesti);
			
			//Rebent dades sobre el DatagramPacket
			lsocket.receive(datagrama);

			//Tancament del socket
			if(!lsocket.isClosed())
				lsocket.close();

			//Visualització del text rebut del DatagramPacket
			System.out.print("Text rebut: " + new String(datagrama.getData(), 0, datagrama.getLength(), "UTF-8"));

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}//main
}//class
