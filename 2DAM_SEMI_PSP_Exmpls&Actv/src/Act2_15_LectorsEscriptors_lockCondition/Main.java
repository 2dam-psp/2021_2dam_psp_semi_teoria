package Act2_15_LectorsEscriptors_lockCondition;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		BaseDades bd = new BaseDades();
		 Lector[]   lectors = new Lector[3];
		 Escriptor[] escriptors = new Escriptor[2];
		 
			System.out.println("[" +Thread.currentThread().getName()
					+"] Programa LECTORS/ESCRIPTORS amb "+lectors.length 
					+ " lectors i "+escriptors.length+ " escriptors");
			System.out.println("-----------------------------------------------------------");
			
			//creacio ojectes Runnable
			for(int i = 0; i < lectors.length; i++)
				lectors[i] = new Lector(2,bd);
			for(int i = 0; i < escriptors.length; i++)
				escriptors[i] = new Escriptor(2,bd);

			//creacio fils i llançament
			for (int i = 0; i < escriptors.length; i++)
				new Thread(escriptors[i],"E"+(i+1)).start();
			for (int i = 0; i < lectors.length; i++)
				new Thread(lectors[i],"L"+(i+1)).start();
		}//main
	}//class

