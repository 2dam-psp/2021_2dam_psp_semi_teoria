package Exemple2_19_Prioritats;

public class Main {

	public static void main(String[] args) {
		try {

			int numPrioritats = Thread.MAX_PRIORITY;
			int prioritats[] = new int[numPrioritats];
			Thread fils[] = new Thread[numPrioritats];

			int prioritat = Thread.MIN_PRIORITY;
			//Per a totes les prioritats
			for (int i=0; i<numPrioritats; i++, prioritat++) {
				//Crea un vector de prioritats amb totes les prioritats possibles
				prioritats[i] = prioritat;
				//Crea un vector de fils
				fils[i] = new Thread(new FilComptador());
				//Assigna a cada fil una prioritat
				fils[i].setPriority(prioritats[i]);
			}//for

			//inicia les fils 
			for (int i=0; i<numPrioritats; i++) 
				fils[i].start();

			int tempsPausa = 10;
			System.out.println("Esperant " + tempsPausa + " secs...");
			Thread.sleep(tempsPausa * 1000);

			for (int i=0; i<numPrioritats; i++) 
				fils[i].interrupt();	//interrupcio dels fils

		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}//main
}//class
