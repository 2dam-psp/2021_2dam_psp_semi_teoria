package Exemple2_19_Prioritats;

public class FilComptador implements Runnable{
	//attr
	long comptador;

	//Constructor
	FilComptador () {
		this.comptador = 0;
	}
	
	public void run() {
		System.out.println("Executant el fil ["+  Thread.currentThread().getName()+"] amb prioritat " + Thread.currentThread().getPriority());
		while(!Thread.currentThread().isInterrupted()) {
			incrementaComptador();
			Thread.yield();
		}//while
		System.out.println("["+ Thread.currentThread().getName()+ "] amb prioritat " + Thread.currentThread().getPriority()+ " disposa d'un comptador amb valor final "+ getComptador() );
	}//run

	//-----------------------------------------
	public long getComptador() {
		return comptador;
	}//getComptador

	//-----------------------------------------
	public void incrementaComptador() {
		this.comptador++;
	}//incrementaComptador
}//class
